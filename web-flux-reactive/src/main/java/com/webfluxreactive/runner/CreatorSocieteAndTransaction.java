package com.webfluxreactive.runner;

import com.webfluxreactive.model.Societe;
import com.webfluxreactive.model.Transaction;
import com.webfluxreactive.repository.SocieteRepository;
import com.webfluxreactive.repository.TransactionRepository;
import lombok.AllArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.stream.Stream;

@Component @AllArgsConstructor
public class CreatorSocieteAndTransaction implements CommandLineRunner {

    private final SocieteRepository societeRepository;
    private final TransactionRepository transactionRepository;

    @Override
    public void run(String... args) throws Exception {

        societeRepository.deleteAll().subscribe(null,null, ()-> {
            Stream.of("SG", "AWB", "BMCE", "AXA").forEach(s-> {
                societeRepository.save(new Societe(s,s,100+Math.random()*900))
                        .subscribe(soc-> {
                            System.out.println(soc.toString());
                        });
            });

            transactionRepository.deleteAll().subscribe(null, null, ()-> {
                Stream.of("SG", "AWB", "BMCE", "AXA").forEach(s-> {
                    societeRepository.findById(s).subscribe(soc->{
                        for (int i = 0; i < 10; i++) {
                            Transaction transaction = Transaction.builder()
                                    .instant(Instant.now())
                                    .societe(soc)
                                    .price(soc.getPrice()*(1+(Math.random()*12-6)/100)).build();
                            transactionRepository.save(transaction).subscribe(t->{
                                System.out.println(t.toString());
                            });
                        }
                    });
                });
            });
        });

        System.out.println("............");

    }
}
