package com.webfluxreactive.repository;

import com.webfluxreactive.model.Societe;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

public interface SocieteRepository extends ReactiveMongoRepository<Societe, String> {
}
