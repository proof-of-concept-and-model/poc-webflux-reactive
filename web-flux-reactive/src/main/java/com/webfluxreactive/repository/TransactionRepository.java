package com.webfluxreactive.repository;

import com.webfluxreactive.model.Societe;
import com.webfluxreactive.model.Transaction;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;

public interface TransactionRepository extends ReactiveMongoRepository<Transaction, String> {

    public Flux<Transaction> findBySociete_Name(String Societe_Name);
}
