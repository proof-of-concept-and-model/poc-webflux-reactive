package com.webfluxreactive.controller;

import com.webfluxreactive.repository.SocieteRepository;
import com.webfluxreactive.repository.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class WebMVCController {

    @Autowired
    private SocieteRepository societeRepository;

    @Autowired
    private TransactionRepository transactionRepository;

    @GetMapping(value = "/index")
    public String index(Model model) {

        model.addAttribute("societes", societeRepository.findAll());
        return "index";
    }
}
