package com.webfluxreactive.controller;

import com.webfluxreactive.model.Event;
import com.webfluxreactive.model.Transaction;
import com.webfluxreactive.repository.SocieteRepository;
import com.webfluxreactive.repository.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import java.time.Duration;
import java.time.Instant;
import java.util.stream.Stream;

@RestController
public class TransactionReactiveRestController {

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private SocieteRepository societeRepository;

    @GetMapping(value = "/transactions")
    public Flux<Transaction> findAll() {
        return transactionRepository.findAll();
    }

    @GetMapping(value = "/transaction/{id}")
    public Mono<Transaction> getOne(@PathVariable String id) {
        return transactionRepository.findById(id);
    }

    @PostMapping(value = "/transaction")
    public Mono<Transaction> save(@RequestBody Transaction transaction) {
        return transactionRepository.save(transaction);
    }

    @DeleteMapping(value = "/transaction/{id}")
    public Mono<Void> delete(@PathVariable String id) {
        return transactionRepository.deleteById(id);
    }

    @PutMapping(value = "/transaction/{id}")
    public Mono<Transaction> update(@RequestBody Transaction transaction, @PathVariable String id) {
        transaction.setId(id);
        return transactionRepository.save(transaction);
    }

    @GetMapping(value = "/streamTransactions", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<Transaction> streamTransactions() {
        return transactionRepository.findAll();
    }

    @GetMapping(value = "/transactionsBySociete/{name}")
    public Flux<Transaction> transactionsBySociete(@PathVariable String name) {
        return transactionRepository.findBySociete_Name(name);
    }

    @GetMapping(value = "/streamTransactionsBySociete/{id}", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<Transaction> streamTransactionsBySociete(@PathVariable String id) {

        return societeRepository.findById(id)
                .flatMapMany(soc->{
            Flux<Long> interval=Flux.interval(Duration.ofMillis(1000));
            Flux<Transaction> transactionFlux= Flux.fromStream(Stream.generate(()-> {
                Transaction transaction = Transaction.builder()
                        .instant(Instant.now())
                        .societe(soc)
                        .price(soc.getPrice()*(1+(Math.random()*12-6)/100)).build();
                return transaction;
            }));
            return Flux.zip(interval,transactionFlux)
                    .map(data-> {
                        return data.getT2();
                    }).share();
        });
    }

    @GetMapping(value = "/events/{id}", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<Double> events(@PathVariable String id) {
        WebClient webClient=WebClient.create("http://localhost:8082");

        Flux<Double> eventFlux=webClient.get()
                .uri("/streamEvents/" + id)
                .retrieve().bodyToFlux(Event.class)
                .map(data->data.getValue());
        return eventFlux;
    }

    @GetMapping(value = "/test")
    public String test() {
        return Thread.currentThread().getName();
    }
}
