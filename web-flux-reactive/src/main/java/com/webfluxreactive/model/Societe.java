package com.webfluxreactive.model;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@Data @AllArgsConstructor @Getter @Setter @NoArgsConstructor @Builder @ToString
public class Societe {

    @Id
    private String id;
    private String name;
    private Double price;

}
