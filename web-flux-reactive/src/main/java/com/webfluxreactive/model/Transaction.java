package com.webfluxreactive.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import java.time.Instant;

@Document
@Data @AllArgsConstructor @NoArgsConstructor @Getter @Setter @Builder @ToString
public class Transaction {

    @Id
    private String id;
    private Instant instant;
    private Double price;

//    @DBRef
//    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Societe societe;

}
