package com.webfluxreactive.model;

import lombok.*;

import java.time.Instant;

@Data
public class Event {

    private Instant instant;
    private Double value;
    private String societeID;

}
