package com.eventservice;

import java.time.Instant;

public class Event {

    private Instant instant;
    private Double value;
    private String societeID;

    public Event() {
    }

    public Event(Instant instant, Double value, String societeID) {
        this.instant = instant;
        this.value = value;
        this.societeID = societeID;
    }

    public Instant getInstant() {
        return instant;
    }

    public void setInstant(Instant instant) {
        this.instant = instant;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public String getSocieteID() {
        return societeID;
    }

    public void setSocieteID(String societeID) {
        this.societeID = societeID;
    }
}
